package com.ji.ui;

import java.awt.*;

import javax.swing.*;

public class Search extends JPanel {
	
	private JLabel lbl;
	private JLabel lb2;
	private JPanel panel1;
	private JPanel panel2;
	
	private JLabel select;
	private JLabel from;
	private JLabel where;
	private JLabel select2;
	private JLabel from2;
	private JLabel where2;
	private JLabel in;
	private JLabel select3;
	private JLabel from3;
	private JLabel where3;
	private JLabel groupBy;
	private JLabel having;
	
	private JTextField Condition;//조건
	private JTextField TableName;//테이블이름
	private JTextField Select;//고른거
	private JTextField IN;//고른거
	private JTextField Condition2;//조건
	private JTextField TableName2;//테이블이름
	private JTextField Select2;//고른거
	
	private JTextField Condition3;//조건
	private JTextField TableName3;//테이블이름
	private JTextField Select3;//고른거
	private JTextField GroupBy;
	private JTextField Having;
	
	private JButton searchbtn;
	private JButton searchbtn2;
	
	
	public Search() {
		setPreferredSize(new Dimension(1000,600));
		setBackground(Color.WHITE);
		
		panel1 = new JPanel();
		panel1.setLayout(null);
		panel1.setBounds(50,50,1000,200);
		
		lbl = new JLabel("내포질의 / 질의문");
		lbl.setFont(new Font("",Font.BOLD, 20));
		lbl.setBounds(5,0,500,30);
		
		select = new JLabel("SELECT ");
		select.setFont(new Font("",Font.PLAIN, 15));
		select.setBounds(5,50,70,30);
		Condition = new JTextField();
		Condition.setBounds(80,50,130,30);
		
		from = new JLabel("FROM ");
		from.setFont(new Font("",Font.PLAIN, 15));
		from.setBounds(250,50,70,30);
		TableName = new JTextField();
		TableName.setBounds(325,50,130,30);
		
		where = new JLabel("WHERE ");
		where.setFont(new Font("",Font.PLAIN, 15));
		where.setBounds(500,40,80,30);
		Select = new JTextField();
		Select.setBounds(580,50,130,30);
		
		in = new JLabel("IN");
		in.setFont(new Font("",Font.PLAIN, 15));
		in.setBounds(750,50,50,30);
		IN = new JTextField();
		IN.setBounds(810,50,130,30);
		
		select2 = new JLabel("SELECT");
		select2.setFont(new Font("",Font.PLAIN, 15));
		select2.setBounds(5,100,70,30);
		Condition2 = new JTextField();
		Condition2.setBounds(80,100,130,30);
		
		from2 = new JLabel("FROM");
		from2.setFont(new Font("",Font.PLAIN, 15));
		from2.setBounds(250,100,70,30);
		TableName2 = new JTextField();
		TableName2.setBounds(325,100,130,30);
		
		where2 = new JLabel("WHERE");
		where2.setFont(new Font("",Font.PLAIN, 15));
		where2.setBounds(500,100,80,30);
		Select2 = new JTextField();
		Select2.setBounds(580,100,130,30);
		
		searchbtn = new JButton("SEARCH");
		searchbtn.setBounds(450,150,100,30);
		
		add(panel1);
		panel1.add(lbl);
		panel1.add(select);
		panel1.add(from);
		panel1.add(where);
		panel1.add(in);
		panel1.add(select2);
		panel1.add(from2);
		panel1.add(where2);
		panel1.add(searchbtn);
		panel1.add(Condition);
		panel1.add(TableName);
		panel1.add(Select);
		panel1.add(IN);
		panel1.add(Condition2);
		panel1.add(TableName2);
		panel1.add(Select2);
		
		
		panel2 = new JPanel();
		panel2.setLayout(null);
		panel2.setBounds(50,280,1000,200);
		
		lb2 = new JLabel("GROUP BY / HAVING");
		lb2.setFont(new Font("",Font.BOLD, 20));
		lb2.setBounds(5,0,250,40);
		
		select3 = new JLabel("SELECT");
		select3.setFont(new Font("",Font.PLAIN, 15));
		select3.setBounds(5,50,70,30);
		Condition3 = new JTextField();
		Condition3.setBounds(80,50,130,30);
		
		from3 = new JLabel("FROM");
		from3.setFont(new Font("",Font.PLAIN, 15));
		from3.setBounds(250,50,70,30);
		TableName3 = new JTextField();
		TableName3.setBounds(325,50,130,30);
		
		where3 = new JLabel("WHERE");
		where3.setFont(new Font("",Font.PLAIN, 15));
		where3.setBounds(500,50,80,30);
		Select3 = new JTextField();
		Select3.setBounds(580,50,250,30);
		
		groupBy = new JLabel("GROUP BY");
		groupBy.setFont(new Font("",Font.PLAIN, 15));
		groupBy.setBounds(5,100,70,30);
		GroupBy = new JTextField();
		GroupBy.setBounds(80,100,375,30);
		
		having = new JLabel("HAVING");
		having.setFont(new Font("",Font.PLAIN, 15));
		having.setBounds(500,100,80,30);
		Having = new JTextField();
		Having.setBounds(580,100,250,30);
		
		searchbtn2 = new JButton("SEARCH");
		searchbtn2.setBounds(450,150,100,30);
		
		add(panel2);
		panel2.add(lb2);
		panel2.add(select3);
		panel2.add(from3);
		panel2.add(where3);
		panel2.add(groupBy);
		panel2.add(having);
		panel2.add(Condition3);
		panel2.add(TableName3);
		panel2.add(Select3);
		panel2.add(GroupBy);
		panel2.add(Having);
		panel2.add(searchbtn2);
		
	}
}
