package com.ji.ui;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;


public class PrimaryPanel extends JPanel {

	private AddDeleteUpdate addDeleteUpdatePanel;
	private Search searchPanel;
	private ShowAll showallPanel;
	private ShowAllManager showallMPanel;
	private String[] table= {"SEARCH","ADD_DELETE_UPDATE"};
	private String[] mode = {"User","Manager"};
	private JComboBox<String> tableBox,modeBox;
	private JButton showallBtn;
	private AddItemListener itemL;
	private BtnListener btnL;

	public PrimaryPanel() {
		setPreferredSize(new Dimension(1200,800));
		setLayout(null);
		setBounds(0,0,1200,800);
		setBackground(Color.LIGHT_GRAY);
		
		itemL = new AddItemListener();
		btnL = new BtnListener();

		tableBox = new JComboBox<String>(table);
		tableBox.setLayout(null);
		tableBox.setBounds(50, 50, 150, 50);
		tableBox.addItemListener(itemL);
		add(tableBox);

		modeBox = new JComboBox<String>(mode);
		modeBox.setLayout(null);
		modeBox.setBounds(205, 50, 70, 50);
		modeBox.addItemListener(itemL);
		add(modeBox);
		modeBox.setVisible(false);

		showallBtn = new JButton("SHOW ALL");
		showallBtn.setLayout(null);
		showallBtn.setBounds(280,50,100,50);
		showallBtn.addActionListener(btnL);
		add(showallBtn);

		addDeleteUpdatePanel = new AddDeleteUpdate();
		addDeleteUpdatePanel.setLayout(null);
		addDeleteUpdatePanel.setBounds(50,100,1100,650);
		add(addDeleteUpdatePanel);
		addDeleteUpdatePanel.setVisible(false);

		searchPanel = new Search();
		searchPanel.setLayout(null);
		searchPanel.setBounds(50,100,1100,650);
		add(searchPanel);
		searchPanel.setVisible(true);
		
		showallPanel = new ShowAll();
		showallPanel.setLayout(null);
		showallPanel.setBounds(50,100,1100,650);
		add(showallPanel);
		showallPanel.setVisible(false);
		
		showallMPanel = new ShowAllManager();
		showallMPanel.setLayout(null);
		showallMPanel.setBounds(50,100,1100,650);
		add(showallMPanel);
		showallMPanel.setVisible(false);
	}


	private class AddItemListener implements ItemListener{
		public void itemStateChanged(ItemEvent e) {
			Object cb = e.getItem();
			if(cb.equals("SEARCH")) {
				System.out.println("search 선택");
				modeBox.setVisible(false);
				addDeleteUpdatePanel.setVisible(false);
				showallPanel.setVisible(false);
				searchPanel.setVisible(true);
			}
			if(cb.equals("ADD_DELETE_UPDATE")) {
				System.out.println("add_update_delete 선택");
				modeBox.setVisible(false);
				searchPanel.setVisible(false);
				showallPanel.setVisible(false);
				addDeleteUpdatePanel.setVisible(true);
			}
			if(cb.equals("User")) {
				showallPanel.setVisible(true);
				showallMPanel.setVisible(false);
			}
			if(cb.equals("Manager")) {
				showallPanel.setVisible(false);
				showallMPanel.setVisible(true);
			}
		}//콤보박스 선택시 바로 패널 이동
	}

	 private class BtnListener implements ActionListener{
         public void actionPerformed(ActionEvent event) {
            Object obj=event.getSource();
            if(obj==showallBtn) { // 시작 버튼을 눌렀을때 게임 시작하는 화면(startAction())으로 옮겨감
            	searchPanel.setVisible(false);
            	addDeleteUpdatePanel.setVisible(false);
				showallPanel.setVisible(true);
				modeBox.setVisible(true);
             }
            
         }
      }
}
