package com.ji.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class App extends JPanel{
	
	public static void main(String[] args) {
		 
		JFrame frame = new JFrame("DATABASE PROJECT");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setResizable(false);//frame 기본 설정
	    
	    PrimaryPanel primary =new PrimaryPanel();// 모든 클래스패널들 모아놓는 클래스를 생성
	    frame.getContentPane().add(primary);
	    frame.pack();
	    frame.setVisible(true);//frame 기본 설정
	   }
}
