package com.ji.ui;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import com.ji.model.dao.CompanyDAO;
import com.ji.model.dao.RentDAO;
import com.ji.model.dao.RepairDAO;

public class AddDeleteUpdate extends JPanel {
	
	private JPanel addPanel,deletePanel,updatePanel;
	
	private JLabel addLbl,deleteLbl,updateLbl;
	private JLabel compIdLbl,compNameLbl,compAddressLbl,compNumberLbl,masterNameLbl,compEmailLbl;
	private JLabel rentIdLbl;
	private JLabel driveLicenseIdLbl, cusNameLbl, cusAddressLbl, cusNumberLbl, cusEmailLbl, beforeUsedLbl, beforeUsedCarLbl;
	
	private JTextField compIdTf,compNameTf,compAddressTf,compNumberTf,masterNameTf,compEmailTf;
	private JTextField rentTf;
	private JTextField driveLicenseIdTf, cusNameTf, cusAddressTf, cusNumberTf, cusEmailTf, beforeUsedTf, beforeUsedCarTf;
	
	private JButton addBtn,deleteBtn,updateBtn,setupBtn;
	private BtnListener btnL;
	
	public AddDeleteUpdate() {
		setPreferredSize(new Dimension(1000,600));
		setBackground(Color.WHITE);
		
		btnL =  new BtnListener();
		
		//insert to company
		addPanel = new JPanel();
		addPanel.setLayout(null);
		addPanel.setBounds(50,50,1000,200);
		add(addPanel);
		
		addLbl = new JLabel("ADD TO COMPANY");
		addLbl.setFont(new Font("",Font.BOLD,20));
		addLbl.setLayout(null);
		addLbl.setBounds(10,20,250,20);
		addPanel.add(addLbl);
		
		addBtn = new JButton("ADD");
		addBtn.setFont(new Font("",Font.PLAIN,15));
		addBtn.setLayout(null);
		addBtn.setBounds(860,170,100,20);
		addPanel.add(addBtn);
		
		compIdLbl = new JLabel("ID");
		compIdLbl.setFont(new Font("",Font.PLAIN,15));
		compIdLbl.setLayout(null);
		compIdLbl.setBounds(10,70,100,30);
		addPanel.add(compIdLbl);
		
		compNameLbl = new JLabel("NAME");
		compNameLbl.setFont(new Font("",Font.PLAIN,15));
		compNameLbl.setLayout(null);
		compNameLbl.setBounds(310,70,100,30);
		addPanel.add(compNameLbl);
		
		compAddressLbl = new JLabel("ADDRESS");
		compAddressLbl.setFont(new Font("",Font.PLAIN,15));
		compAddressLbl.setLayout(null);
		compAddressLbl.setBounds(660,70,100,30);
		addPanel.add(compAddressLbl);
		
		compNumberLbl = new JLabel("NUMBER");
		compNumberLbl.setFont(new Font("",Font.PLAIN,15));
		compNumberLbl.setLayout(null);
		compNumberLbl.setBounds(10,120,100,30);
		addPanel.add(compNumberLbl);
		
		masterNameLbl = new JLabel("MASTERNAME");
		masterNameLbl.setFont(new Font("",Font.PLAIN,15));
		masterNameLbl.setLayout(null);
		masterNameLbl.setBounds(310,120,100,30);
		addPanel.add(masterNameLbl);
		
		compEmailLbl = new JLabel("EAMAIL");
		compEmailLbl.setFont(new Font("",Font.PLAIN,15));
		compEmailLbl.setLayout(null);
		compEmailLbl.setBounds(660,120,100,30);
		addPanel.add(compEmailLbl);
		
		compIdTf = new JTextField();
		compIdTf.setLayout(null);
		compIdTf.setBounds(110,70,120,30);
		addPanel.add(compIdTf);
		
		compNameTf = new JTextField();
		compNameTf.setLayout(null);
		compNameTf.setBounds(460,70,120,30);
		addPanel.add(compNameTf);
		
		compAddressTf = new JTextField();
		compAddressTf.setLayout(null);
		compAddressTf.setBounds(760,70,200,30);
		addPanel.add(compAddressTf);
		
		compNumberTf = new JTextField();
		compNumberTf.setLayout(null);
		compNumberTf.setBounds(110,120,120,30);
		addPanel.add(compNumberTf);
		
		masterNameTf = new JTextField();
		masterNameTf.setLayout(null);
		masterNameTf.setBounds(460,120,120,30);
		addPanel.add(masterNameTf);
		
		compEmailTf = new JTextField();
		compEmailTf.setLayout(null);
		compEmailTf.setBounds(760,120,200,30);
		addPanel.add(compEmailTf);

		
		//delete from rent
		deletePanel = new JPanel();
		deletePanel.setLayout(null);
		deletePanel.setBounds(50,270,1000,110);
		add(deletePanel);
		
		deleteLbl = new JLabel("DELETE FROM RENT");
		deleteLbl.setFont(new Font("",Font.BOLD,20));
		deleteLbl.setLayout(null);
		deleteLbl.setBounds(10,20,300,20);
		deletePanel.add(deleteLbl);

		deleteBtn = new JButton("DELETE");
		deleteBtn.setFont(new Font("",Font.PLAIN,15));
		deleteBtn.setLayout(null);
		deleteBtn.setBounds(310,80,100,20);
		deletePanel.add(deleteBtn);
		
		setupBtn = new JButton("SETUP");
		setupBtn.setFont(new Font("",Font.PLAIN,15));
		setupBtn.setLayout(null);
		setupBtn.setBounds(440,80,100,20);
		setupBtn.addMouseListener(btnL);
		deletePanel.add(setupBtn);
		//초기화 경고창 추가,,,
		
		rentIdLbl = new JLabel("ID");
		rentIdLbl.setFont(new Font("",Font.PLAIN,15));
		rentIdLbl.setLayout(null);
		rentIdLbl.setBounds(10,70,100,30);
		deletePanel.add(rentIdLbl);
		
		rentTf = new JTextField();
		rentTf.setLayout(null);
		rentTf.setBounds(70,70,100,30);
		deletePanel.add(rentTf);
		
		
		//update from customer
		updatePanel = new JPanel();
		updatePanel.setLayout(null);
		updatePanel.setBounds(50,400,1000,220);
		add(updatePanel);
		
		updateLbl = new JLabel("UPDATE CUSTOMER");
		updateLbl.setFont(new Font("",Font.BOLD,20));
		updateLbl.setLayout(null);
		updateLbl.setBounds(10,20,250,20);
		updatePanel.add(updateLbl);
		
		updateBtn = new JButton("UPDATE");
		updateBtn.setFont(new Font("",Font.PLAIN,15));
		updateBtn.setLayout(null);
		updateBtn.setBounds(860,170,100,20);
		updatePanel.add(updateBtn);
	
		driveLicenseIdLbl = new JLabel("DRIVER_ID");
		driveLicenseIdLbl.setFont(new Font("",Font.PLAIN,15));
		driveLicenseIdLbl.setLayout(null);
		driveLicenseIdLbl.setBounds(10,70,150,30);
		updatePanel.add(driveLicenseIdLbl);
		
		cusNameLbl = new JLabel("NAME");
		cusNameLbl.setFont(new Font("",Font.PLAIN,15));
		cusNameLbl.setLayout(null);
		cusNameLbl.setBounds(310,70,100,30);
		updatePanel.add(cusNameLbl);
		
		cusAddressLbl = new JLabel("ADDRESS");
		cusAddressLbl.setFont(new Font("",Font.PLAIN,15));
		cusAddressLbl.setLayout(null);
		cusAddressLbl.setBounds(660,70,100,30);
		updatePanel.add(cusAddressLbl);
		
		cusNumberLbl = new JLabel("NUMBER");
		cusNumberLbl.setFont(new Font("",Font.PLAIN,15));
		cusNumberLbl.setLayout(null);
		cusNumberLbl.setBounds(10,120,100,30);
		updatePanel.add(cusNumberLbl);
		
		cusEmailLbl = new JLabel("EAMAIL");
		cusEmailLbl.setFont(new Font("",Font.PLAIN,15));
		cusEmailLbl.setLayout(null);
		cusEmailLbl.setBounds(310,120,100,30);
		updatePanel.add(cusEmailLbl);
		
		beforeUsedLbl = new JLabel("BEFORE_USED_DATA");
		beforeUsedLbl.setFont(new Font("",Font.PLAIN,15));
		beforeUsedLbl.setLayout(null);
		beforeUsedLbl.setBounds(10,170,170,30);
		updatePanel.add(beforeUsedLbl);
		
		beforeUsedCarLbl = new JLabel("BEFORE_USED_CAR");
		beforeUsedCarLbl.setFont(new Font("",Font.PLAIN,15));
		beforeUsedCarLbl.setLayout(null);
		beforeUsedCarLbl.setBounds(460,170,150,30);
		updatePanel.add(beforeUsedCarLbl);
		 
			
		driveLicenseIdTf = new JTextField();
		driveLicenseIdTf.setLayout(null);
		driveLicenseIdTf.setBounds(110,70,120,30);
		updatePanel.add(driveLicenseIdTf);
		
		cusNameTf = new JTextField();
		cusNameTf.setLayout(null);
		cusNameTf.setBounds(460,70,120,30);
		updatePanel.add(cusNameTf);
		
		cusAddressTf = new JTextField();
		cusAddressTf.setLayout(null);
		cusAddressTf.setBounds(760,70,200,30);
		updatePanel.add(cusAddressTf);
		
		cusNumberTf = new JTextField();
		cusNumberTf.setLayout(null);
		cusNumberTf.setBounds(110,120,120,30);
		updatePanel.add(cusNumberTf);
		
		cusEmailTf = new JTextField();
		cusEmailTf.setLayout(null);
		cusEmailTf.setBounds(460,120,200,30);
		updatePanel.add(cusEmailTf);
		
		beforeUsedTf = new JTextField();
		beforeUsedTf.setLayout(null);
		beforeUsedTf.setBounds(180,170,170,30);
		updatePanel.add(beforeUsedTf);

		beforeUsedCarTf = new JTextField();
		beforeUsedCarTf.setLayout(null);
		beforeUsedCarTf.setBounds(620,170,150,30);
		updatePanel.add(beforeUsedCarTf);
		
	}
	
	private class BtnListener implements MouseListener{
        public void mouseClicked(MouseEvent event) {}
        public void mousePressed(MouseEvent event) {}
        public void mouseReleased(MouseEvent event) {
        	JButton btn = (JButton)event.getSource();

        	if(btn == setupBtn) {
        		JOptionPane.showMessageDialog(null, "초기화 하시겠습니까?");
        	}
        	if(btn == addBtn) {
        		//company table
        		CompanyDAO comp = CompanyDAO.getInstance();
        		int compId = Integer.parseInt(compIdLbl.getText());
        		String compName = compNameLbl.getText();
        		String compAddress = compAddressLbl.getText();
        		String compNumber = compNumberLbl.getText();
        		String masterName = masterNameLbl.getText();
        		String compEmail = compEmailLbl.getText();
        		
        		comp.insertCompany(compId,compName,compAddress,compNumber,compEmail,masterName);
        	}
        	if(btn == deleteBtn) {
        		RentDAO rent = RentDAO.getInstance();
        		String rentId = rentIdLbl.getText();

        		rent.deleteRent(rentId);
        	}
        	if(btn == updateBtn) {

        	}
        	

        }
        public void mouseEntered(MouseEvent event) {}
        public void mouseExited(MouseEvent event) {}         
     }//BtnMenuListener
	
}
