package com.ji.model.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ji.model.DatabaseConnect;
import com.ji.model.vo.CarVO;
import com.ji.model.vo.CustomerVO;

public class CustomerDAO {
	//싱글톤 클래스로 만들기 위해서 생성자를 private로 선언
	private CustomerDAO() {}
	//하나의 객체를 주소로 저장할 변수 선언
	private static CustomerDAO obj;
	//선언한 static변수에 객체를 생성해주는 메서드 선언
	public static CustomerDAO getInstance() {
		if(obj==null) {
			obj=new CustomerDAO();
		}
		return obj;
	}
		
	private Connection conn = DatabaseConnect.getConnection();
	
	public ArrayList<CustomerVO> selectCustomer(String condition){
		String sql = "select * from Customer";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		ArrayList<CustomerVO> arrCustomerVO = new ArrayList<CustomerVO>();
		
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				CustomerVO tempCustomerVO = new CustomerVO(
						rs.getInt("driveLicneseID"),
						rs.getString("name"),
						rs.getString("address"),
						rs.getString("number"),
						rs.getString("email"),
						rs.getString("beforeUsed"),
						rs.getString("beforeUsedCar"));
				arrCustomerVO.add(tempCustomerVO);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return arrCustomerVO;
	}//insert
	
	public ArrayList<CustomerVO> subQueryCar(String select1, String select2, String from1, String from2, 
			String where1, String where2, String in){
		String sql = "select "+select1+" from "+from1+" where "+where1;
		if(   !(in == null || in.equals(""))   ) {
			sql += " in "+in; //where절
		}
		if(   !(select2 == null || select2.equals(""))   ) {
			sql += " (select "+select2; //where절
		}
		if(   !(from2 == null || from2.equals(""))   ) {
			sql += " from "+from2; //where절
		}
		if(   !(where2 == null || where2.equals(""))   ) {
			sql += " where "+where2+");"; //where절
		}
		
		PreparedStatement pstmt = null;
		
		ArrayList<CustomerVO> arrCustomerVO = new ArrayList<CustomerVO>();
		
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				CustomerVO tempCustomerVO = new CustomerVO(
						rs.getInt("driveLicneseID"),
						rs.getString("name"),
						rs.getString("address"),
						rs.getString("number"),
						rs.getString("email"),
						rs.getString("beforeUsed"),
						rs.getString("beforeUsedCar"));
				arrCustomerVO.add(tempCustomerVO);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return arrCustomerVO;
	}//sub_query
	
	public void insertCustomer(int driveLicenseId, String name, String address, String number,
			 String email, String beforeUsed, String beforeUsedCar){
		
		String sql = "insert into Customer values (?,?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,driveLicenseId);
			pstmt.setString(2,name);
			pstmt.setString(3,address);
			pstmt.setString(4,number);
			pstmt.setString(5,email);
			pstmt.setString(6,beforeUsed);
			pstmt.setString(7,beforeUsedCar);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void updateCustomer(int driveLicenseId, String name, String address, String num,
			 String email, String beforeUsed, String beforeUsedCar,String condition){
		
		String sql = "update Customer set  (?,?,?,?,?,?,?)";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,driveLicenseId);
			pstmt.setString(2,name);
			pstmt.setString(3,address);
			pstmt.setString(4,num);
			pstmt.setString(5,email);
			pstmt.setString(6,beforeUsed);
			pstmt.setString(7,beforeUsedCar);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void deleteCustomer(int driveLicenseId,String condition){
		
		String sql = "delete from Customer";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,driveLicenseId);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

	}
}
