package com.ji.model.dao;

import java.sql.*;
import java.util.ArrayList;

import com.ji.model.DatabaseConnect;
import com.ji.model.vo.RequestVO;

public class RequestDAO {
	//싱글톤 클래스로 만들기 위해서 생성자를 private로 선언
	private RequestDAO() {}
	//하나의 객체를 주소로 저장할 변수 선언
	private static RequestDAO obj;
	//선언한 static변수에 객체를 생성해주는 메서드 선언
	public static RequestDAO getInstance() {
		if(obj==null) {
			obj=new RequestDAO();
		}
		return obj;
	}

	private Connection conn = DatabaseConnect.getConnection();
	
	public ArrayList<RequestVO> selectRequest(String condition){
		String sql = "select * from Request";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		ArrayList<RequestVO> arrRequestVO = new ArrayList<RequestVO>();
		
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				RequestVO tempRequestVO = new RequestVO(rs.getInt("requestId"),
						rs.getInt("carId"),
						rs.getInt("compId"),
						rs.getInt("repairId"),
						rs.getInt("driveLicenseId"),
						rs.getString("requestHistory"),
						rs.getString("requestDate"),
						rs.getInt("requestFee"),
						rs.getString("deadline"),
						rs.getString("addtionalRequest"));
				arrRequestVO.add(tempRequestVO);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return arrRequestVO;
	}//insert
	
	public void insertRequest(int requestId, int carId, int compId, int repairId,
			int driveLicenseId, String requestHistory, String requestDate,
			int requestFee, String deadline, String additionalRequest){
		
		String sql = "insert into Request values (?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,requestId);
			pstmt.setInt(2,carId);
			pstmt.setInt(3,compId);
			pstmt.setInt(4,repairId);
			pstmt.setInt(5,driveLicenseId);
			pstmt.setString(6,requestHistory);
			pstmt.setString(7,requestDate);
			pstmt.setInt(8,requestFee);
			pstmt.setString(9,deadline);
			pstmt.setString(10,additionalRequest);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void updateRequest(int requestId, int carId, int compId, int repairId,
			int driveLicenseId, String requestHistory, String requestDate,
			int requestFee, String deadline, String additionalRequest,String condition){
		
		String sql = "update Request set  (?,?,?,?,?,?,?,?,?,?)";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,requestId);
			pstmt.setInt(2,carId);
			pstmt.setInt(3,repairId);
			pstmt.setInt(4,compId);
			pstmt.setInt(5,driveLicenseId);
			pstmt.setString(6,requestHistory);
			pstmt.setString(7,requestDate);
			pstmt.setInt(8,requestFee);
			pstmt.setString(9,deadline);
			pstmt.setString(10,additionalRequest);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void deleteRequest(int requestId,String condition){
		
		String sql = "delete from Request";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,requestId);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
	
}
