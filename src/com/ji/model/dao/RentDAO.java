package com.ji.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ji.model.DatabaseConnect;
import com.ji.model.vo.RentVO;

public class RentDAO {
	//싱글톤 클래스로 만들기 위해서 생성자를 private로 선언
	private RentDAO() {}
	//하나의 객체를 주소로 저장할 변수 선언
	private static RentDAO obj;
	//선언한 static변수에 객체를 생성해주는 메서드 선언
	public static RentDAO getInstance() {
		if(obj==null) {
			obj=new RentDAO();
		}
		return obj;
	}
	
	private Connection conn = DatabaseConnect.getConnection();
	
	public ArrayList<RentVO> selectRent(String condition){
		String sql = "select * from Rent";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		ArrayList<RentVO> arrRentVO = new ArrayList<RentVO>();
		
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				RentVO tempRentVO = new RentVO(rs.getInt("rentId"),
						rs.getInt("driveLicenseId"),
						rs.getInt("carId"),
						rs.getInt("compId"),
						rs.getString("startDate"),
						rs.getInt("totalDate"),
						rs.getInt("fee"),
						rs.getString("deadline"),
						rs.getString("additionalHistory"),
						rs.getInt("additionalFee"));
				arrRentVO.add(tempRentVO);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return arrRentVO;
	}//insert
	
	public void insertRent(int rentId, int driveLicenseId, int carId, int compId,
			String startDate, int totalDate, int fee, String deadline,
			String additionalHistory, int additionalFee){
		
		String sql = "insert into Rent values (?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,rentId);
			pstmt.setInt(2,driveLicenseId);
			pstmt.setInt(3,carId);
			pstmt.setInt(4,compId);
			pstmt.setString(5,startDate);
			pstmt.setInt(6,totalDate);
			pstmt.setInt(7,fee);
			pstmt.setString(8,deadline);
			pstmt.setString(9,additionalHistory);
			pstmt.setInt(10,additionalFee);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void updateRent(int rentId, int driveLicenseId, int carId, int compId,
			String startDate, int totalDate, int fee, String deadline,
			String additionalHistory, int additionalFee,String condition){
		
		String sql = "update Rent set  (?,?,?,?,?,?,?,?,?,?)";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,rentId);
			pstmt.setInt(2,carId);
			pstmt.setInt(3,compId);
			pstmt.setInt(4,driveLicenseId);
			pstmt.setString(5,startDate);
			pstmt.setInt(6,totalDate);
			pstmt.setInt(7,fee);
			pstmt.setString(8,deadline);
			pstmt.setString(9,additionalHistory);
			pstmt.setInt(10,additionalFee);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void deleteRent(String rentId){
		String sql = "delete from Rent";
		
		if(   !( rentId == null || rentId.equals(""))   ) {
			sql += "where rentId ="+ rentId; //where절
		}
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,Integer.parseInt(rentId));
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
