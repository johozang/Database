package com.ji.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ji.model.DatabaseConnect;
import com.ji.model.vo.RepairVO;

public class RepairDAO {
	//싱글톤 클래스로 만들기 위해서 생성자를 private로 선언
	private RepairDAO() {}
	//하나의 객체를 주소로 저장할 변수 선언
	private static RepairDAO obj;
	//선언한 static변수에 객체를 생성해주는 메서드 선언
	public static RepairDAO getInstance() {
		if(obj==null) {
			obj=new RepairDAO();
		}
		return obj;
	}

	private Connection conn = DatabaseConnect.getConnection();
	
	public ArrayList<RepairVO> selectRepair(String condition){
		String sql = "select * from Repair";
		
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		
		
		PreparedStatement pstmt = null;
		
		ArrayList<RepairVO> arrRepairVO = new ArrayList<RepairVO>();
		
		try {
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				RepairVO tempRepairVO = new RepairVO(rs.getInt("repairId"),
						rs.getString("name"),
						rs.getString("address"),
						rs.getString("number"),
						rs.getString("masterName"),
						rs.getString("email"));
				arrRepairVO.add(tempRepairVO);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return arrRepairVO;
	}//insert
	
	public void insertRepair(int repairId, String name, String address, String number,
			 String email, String masterName){
		
		String sql = "insert into Repair values (?,?,?,?,?,?)";
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,repairId);
			pstmt.setString(2,name);
			pstmt.setString(3,address);
			pstmt.setString(4,number);
			pstmt.setString(5,email);
			pstmt.setString(6,masterName);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void updateRepair(int repairId, String name, String address, String number,
			 String email, String masterName,String condition){
		
		String sql = "update Repair set  (?,?,?,?,?,?)";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,repairId);
			pstmt.setString(2,name);
			pstmt.setString(3,address);
			pstmt.setString(4,number);
			pstmt.setString(5,masterName);
			pstmt.setString(6,email);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void deleteRepair(int repairId,String condition){
		
		String sql = "delete from Repair";
		if(   !(condition == null || condition.equals(""))   ) {
			sql += "where "+condition; //where절
		}
		//condition이 없는 경우!!!!!!!!!!! 다 삭제될 수 있으니 ui에서 알림 넣을 것!!
		PreparedStatement pstmt = null;
		
		try {
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1,repairId);
			pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt != null && !pstmt.isClosed())
					pstmt.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
}
