package com.ji.model.vo;

public class CarVO {
	 private int carId;
	 private int compId;
	 private String name;
	 private int carNum;
	 private int carMember;
	 private int rentFee;
	 private int regiDate;
	 private String details;
	 
	 public CarVO() {}
	 
	 public CarVO(int carId, int compId, String name, int carNum, int carMember,
			 int rentFee, int regiDate, String details) {
		 this.carId = carId;
		 this.compId = compId;
		 this.name = name;
		 this.carNum = carNum;
		 this.carMember = carMember;
		 this.rentFee = rentFee;
		 this.regiDate = regiDate;
		 this.details = details;
	 }
	 
	 public int getcarId() {return this.carId;}
	 public void setcarId(int id) {this.carId = id;}
	 
	 public int getcompId() {return this.compId;}
	 public void setcompId(int id) {this.compId = id;}
	 
	 public String getname() {return this.name;}
	 public void setname(String name) {this.name = name;}
	 
	 public int getcarNum() {return this.carNum;}
	 public void setcarNum(int num) {this.carNum = num;}
	 
	 public int getcarMember() {return this.carMember;}
	 public void setcarMember(int num) {this.carMember = num;}

	 public int getrentFee() {return this.rentFee;}
	 public void setrentFee(int fee) {this.rentFee = fee;}

	 public int getregiDate() {return this.regiDate;}
	 public void setregiDate(int date) {this.regiDate = date;}

	 public String getdetails() {return this.details;}
	 public void setdetails(String details) {this.details = details;}
	 
	 public String toString() {
		 return "CarVO [carId="+carId+",compId="+compId+",name="+name+",carNum="+carNum
				 +",carMember="+carMember+",rentFee="+rentFee+",regiDate="+regiDate
				 +",details"+details;
	 }
}
