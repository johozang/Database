package com.ji.model.vo;

public class RentVO {
	private  int rentId;//고유대여번호
	private int driveLicenseId;//운전면허증번호
	private int carId;//캠핑카등록ID
	private int compId;//대여회사ID
	private String startDate;//대여시작일
	private int totalDate;//대여기간 
	private int fee;//청구요금
	private String deadline;//납입기한
	private String additionalHistory;//기타청구내역 
	private int additionalFee;//기타청구요금 
	
	public RentVO(int rentId, int driveLicenseId, int carId, int compId,
			String startDate, int totalDate, int fee, String deadline,
			String additionalHistory, int additionalFee) 
	{
		this.rentId = rentId;
		this.driveLicenseId = driveLicenseId;
		this. carId = carId;
		this.compId = compId;
		this.startDate = startDate;
		this.totalDate = totalDate;
		this.fee = fee;
		this.deadline = deadline;
		this.additionalHistory = additionalHistory;
		this.additionalFee = additionalFee;	
	}//constructor

	public int getRentId() {return rentId;}
	public int getDriveLicenseId() {return driveLicenseId;}
	public int getCarId() {return carId;}
	public int getCompId() {return compId;}
	public String getStartDate() {return startDate;}
	public int getTotalDate() {return totalDate;}
	public int getFee() {return fee;}
	public String getDeadline() {return deadline;}
	public String getAdditionalHistory() {return additionalHistory;}
	public int getAdditionalFee() {return additionalFee;}
	//getter
	public void setRentId(int rentId) {this.rentId = rentId;}
	public void setDriveLicenseId(int driveLicenseId) {this.driveLicenseId = driveLicenseId;}
	public void setCarId(int carId) {this.carId = carId;}
	public void setCompId(int compId) {this.compId = compId;}
	public void setStartDate(String startDate) {this.startDate = startDate;}
	public void setTotalDate(int totalDate) {this.totalDate = totalDate;}
	public void setFee(int fee) {this.fee = fee;}
	public void setDeadline(String deadline) {this.deadline = deadline;}
	public void setAdditionalHistory(String additionalHistory) {this.additionalHistory = additionalHistory;}
	public void setAdditionalFee(int additionalFee) {this.additionalFee = additionalFee;}
	//setter
	
	@Override
	public String toString() {
		return "RentVO ["+  rentId + driveLicenseId + carId + compId + startDate + totalDate + fee + deadline + additionalHistory + additionalFee + "]";
		
	}
	
}

