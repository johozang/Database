package com.ji.model.vo;

public class RequestVO {
	private int requestId;//고유정비번호
	private int carId;//캠핑카등록ID
	private int compId;//캠핑카대여회사ID
	private int repairId;//캠핑카정비소ID
	private int driveLicenseId;//운전면허증번호
	private String requestHistory;//정비(의뢰)내역
	private String requestDate;//수리(의뢰)날짜
	private int requestFee;//수리(의뢰)비용
	private String deadline;//납입기한
	private String additionalRequest;//기타정비(의뢰)내역
	
	public RequestVO(int requestId, int carId, int compId, int repairId,
			int driveLicenseId, String requestHistory, String requestDate,
			int requestFee, String deadline, String additionalRequest) {
		this.requestId = requestId;
		this.carId = carId;
		this.compId = compId;
		this.repairId = repairId;
		this.driveLicenseId = driveLicenseId;
		this.requestHistory = requestHistory;
		this.requestDate = requestDate;
		this.requestFee = requestFee;
		this.deadline = deadline;
		this.additionalRequest = additionalRequest;
	}//constructor

	public int getRequestId() {return requestId;}

	public int getCarId() {return carId;}
	public int getCompId() {return compId;}
	public int getRepairId() {return repairId;}
	public int getDriveLicenseId() {return driveLicenseId;}
	public String getRequestHistory() {return requestHistory;}
	public String getRequestDate() {return requestDate;}
	public int getRequestFee() {return requestFee;}
	public String getDeadline() {return deadline;}
	public String getAdditionalRequest() {return additionalRequest;}
	//getter
	public void setRequestId(int requestId) {this.requestId = requestId;}
	public void setCarId(int carId) {this.carId = carId;}
	public void setCompId(int compId) {this.compId = compId;}
	public void setRepairId(int repairId) {this.repairId = repairId;}
	public void setDriveLicenseId(int driveLicenseId) {this.driveLicenseId = driveLicenseId;}
	public void setRequestHistory(String requestHistory) {this.requestHistory = requestHistory;}
	public void setRequestDate(String requestDate) {this.requestDate = requestDate;}
	public void setRequestFee(int requestFee) {this.requestFee = requestFee;}
	public void setDeadline(String deadline) {this.deadline = deadline;}
	public void setAdditionalRequest(String additionalRequest) {this.additionalRequest = additionalRequest;}
	//setter
	
	@Override
	public String toString() {
		return "RequestVo [" + requestId + carId + compId + repairId + driveLicenseId + requestHistory
				+ requestDate + requestFee + deadline + additionalRequest + "]";
	}
	
	
}
