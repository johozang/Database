package com.ji.model.vo;

public class CustomerVO {
	 private int driveLicenseId;
	 private String name;
	 private String address;
	 private String number;
	 private String email;
	 private String beforeUsed;
	 private String beforeUsedCar;
	 
	 public CustomerVO() {}
	 
	 public CustomerVO(int driveLicenseId, String name, String address, String number,
			 String email, String beforeUsed, String beforeUsedCar) {
		 this.driveLicenseId = driveLicenseId;
		 this.name = name;
		 this.address = address;
		 this.number = number;
		 this.email = email;
		 this.beforeUsed = beforeUsed;
		 this.beforeUsedCar = beforeUsedCar;
	 }
	 
	 public int getdriveLicenseId() {return this.driveLicenseId;}
	 public void setdriveLicenseId(int driveLicenseId) {this.driveLicenseId = driveLicenseId;}

	 public String getname() {return this.name;}
	 public void setname(String name) {this.name = name;}
	 
	 public String getaddress() {return this.address;}
	 public void setaddress(String address) {this.address = address;}
	 
	 public String getnumber() {return this.number;}
	 public void setnumber(String number) {this.number = number;}
	 
	 public String getemail() {return this.email;}
	 public void setemail(String email) {this.email = email;}
	 
	 public String getbeforeUsed() {return this.beforeUsed;}
	 public void setbeforeUsed(String beforeUsed) {this.beforeUsed = beforeUsed;}
	 
	 public String getbeforeUsedCar() {return this.beforeUsedCar;}
	 public void setbeforeUsedCar(String beforeUsedCar) {this.beforeUsedCar = beforeUsedCar;}
	 
	 public String toString() {
		 return "CustomerVO [driveLicenseId="+driveLicenseId+",name="+name+",address="+address+",number="+number
				 +",email="+email+",beforeUsed="+beforeUsed+",beforeUsedCar="+beforeUsedCar;
	 }
}
