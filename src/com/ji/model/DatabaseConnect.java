package com.ji.model;

import java.sql.*;

public class DatabaseConnect {
	
	private static final String USERNAME = "username";
	private static final String PASSWORD = "root";
	private static final String URL = "jdbc:oracle:thin:@localhost:1521:oracl";
	
	//jdbc:DBMS:hostname:port:sid
	
	public static Connection dbConn;
	
	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("oracle.jbdc.driver.OracleDriver");
			conn = DriverManager.getConnection(URL,USERNAME,PASSWORD);
			
			System.out.println("DB Connection OK!");
			
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("DB Driver Error!");
		}catch(SQLException se) {
			se.printStackTrace();
			System.out.println("DB Connection Error");
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Unkonwn Error!");
		}
		return conn;
	}
	
}

